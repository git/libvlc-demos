#ifndef TRACKINFO_H
#define TRACKINFO_H

#include <QString>
#include <QPair>
#include <vlc/vlc.h>
#include "TrackInfo.h"

// Type of a track; Can be audio, video, text or unknown.
class TrackType
{
public:
    TrackType(const libvlc_track_type_t type);                // Type of the track is passed.
    const QString& toString() const { return m_stringtype; }  // Return type of the track as string.
private:
    QString m_stringtype;                                     // Type of the track as string.
};


// Information about a single track.
class TrackInfo
{
public:
    TrackInfo(const libvlc_media_track_info_t info);             // Track-information is passed.
    libvlc_track_type_t vlcType() const { return m_trackinfo.i_type; }  // Return the libVLC-type of the track.
    const TrackType& type() const { return m_type; }             // Return the type of the track.
    uint32_t codec() const { return m_trackinfo.i_codec; }       // Return the codec of the track.
    const QString& codecString() const { return m_codecstring; } // Return the codec as string.
    int id() const { return m_trackinfo.i_id; }                  // Return the ID of the track.
    int profile() const { return m_trackinfo.i_profile; }        // Return the profile of the track.
    int level() const { return m_trackinfo.i_level; }            // Return the level of the track.
    QPair<unsigned int, unsigned int> typeDependentInfo() const; // Return the type-dependent information of the track
                                                                 // (channel and rate for audio; width and height for video)
private:
    libvlc_media_track_info_t m_trackinfo;                       // Track information.
    TrackType m_type;                                            // Type of the track.
    QString m_codecstring;                                       // Codec as string.
};

#endif // TRACKINFO_H
