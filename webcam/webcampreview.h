#ifndef WEBCAMPREVIEW_H
#define WEBCAMPREVIEW_H

#include <QtGui/QWidget>

#include <QtCore/QSet>

#include <vlc/vlc.h>

class VideoFiltersModel;
class WebcamPreview : public QWidget
{
    Q_OBJECT

public:
    WebcamPreview(QWidget* parent = 0);
    ~WebcamPreview();

    void start();
    void stop();

    void takeSnapshot(const QString& filePath);

    void startRecording(const QString& filePath);
    void stopRecording();
    bool isRecording() const { return m_recording; }

    QSize videoSize() const;
    QSize sizeHint() const;

    VideoFiltersModel* videoFiltersModel() const { return m_videoFiltersModel; }

protected slots:
    void enabledFiltersChanged();

private:
    void closeEvent(QCloseEvent* event);

private:
    libvlc_instance_t* m_vlcInstance;
    libvlc_media_player_t* m_vlcMediaPlayer;
    bool m_recording;
    QString m_recordingFilePath;

    VideoFiltersModel* m_videoFiltersModel;
};

#endif // WEBCAMPREVIEW_H
