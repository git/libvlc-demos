#ifndef MEDIAWIDGET_H
#define MEDIAWIDGET_H

#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>
#include <QTreeWidget>
#include <QDateTime>
#include <QFont>
#include "Media.h"

#include <QHeaderView>

// Widget showing information about a single media-file.
class MediaWidget : public QTreeWidget
{
    Q_OBJECT
public:
    MediaWidget(Media *const m, QWidget *parent);     // Media and parent are passed as parameter.
private:
    Media *m_media;                                   // Media to describe.
    QTreeWidgetItem *m_generalnode, *m_tagnode,      // Top-level nodes of the information tree.
    *m_mediainfonode;
    void createTopNodes();                           // Create top-level nodes of the information tree.
    void createGeneralTree(Media *const m);          // Add general information to the information tree.
    void createTagTree(Media *const m);              // Add tags to the general information tree.
    void createMediaInfoTree(Media *const m);        // Add media information to the information tree.
};

#endif // MEDIAWIDGET_H
