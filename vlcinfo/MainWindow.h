#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QMainWindow>
#include <QStackedWidget>
#include "VLC.h"
#include "MainWidgets.h"

// Mainwindow of the application that contains the mainwidget.
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();
public slots:
    void loadFile();                         // Choose a one or more files and show information about it.
    void loadFile(char *filename);  // Show information about the file with the given name.
    void loadDir();                          // Choose directory and show information about all files in it.
    void showStartWidget();                  // Shows the start widget.
    void hideStartWidget();                  // Hide start widget.
private:
    QWidget *m_mainwidget;                   // Mainwidget that contains other widgets.
    QVBoxLayout *m_mainlayout;               // Layout for the mainwidget.
    QHBoxLayout *m_buttonlayout;             // Layout ofr the buttons.
    QPushButton *m_openbutton,*m_opendirbutton,// Buttons to open new media and quit application.
                *m_savebutton, *m_quitbutton;
    TabWidget *m_mediatabs;                 // Widget that contains tabs with all information.
    QLabel *m_label;                        // Widget that shows icons and buttons.
    QStackedWidget *m_stackw;               // Stackwidget change central widget.
    StartWidget *m_startw;                  // Widget displayed at the beginning.
};

#endif // MAINWINDOW_H
