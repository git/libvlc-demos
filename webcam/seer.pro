TARGET = seer

HEADERS += \
    mainwindow.h \
    webcampreview.h \
    videofiltersmodel.h \
    settingsdialog.h

SOURCES += \
    mainwindow.cpp \
    webcampreview.cpp \
    main.cpp \
    videofiltersmodel.cpp \
    settingsdialog.cpp

LIBS += -lvlc
