#include "Application.h"

// Create the application and start the event-loop.
int main(int argc, char *argv[])
{
    return Application(argc, argv).exec();
}


// Check for "-no-gui" or help parameters to create appropriate application.
Application::Application(int argc, char *argv[]) : m_cli(NULL),
                                                   m_app(NULL),
                                                   m_mainwindow(NULL)
{
    for(int i = 1; i < argc; i++)
    {
        if(strcmp("--no-gui", argv[i]) == 0 ||
           strcmp("-h", argv[i]) == 0 ||
           strcmp("--help", argv[i]) == 0)
        {
            m_cli = new Cli(argc, argv);
            return;
        }
    }
    m_app = new QApplication(argc, argv);
    m_mainwindow = new MainWindow();
    for(int i = 1; i < argc; i++)                   // Assume that all parameters are file names and load them.
        m_mainwindow->loadFile(argv[i]);
}


Application::~Application()                         // Clean up.
{
    if(m_cli != NULL)
        delete m_cli;
    else if(m_mainwindow != NULL && m_app != NULL)
    {
        delete m_mainwindow;
        delete m_app;
    }
}


int Application::exec()                             // Execute the desired application.
{
    if(m_cli != NULL)
        return m_cli->exec();
    else if(m_mainwindow != NULL && m_app != NULL)
    {
        m_mainwindow->show();
        return m_app->exec();
    }
    return 1;
}
