#include "settingsdialog.h"

#include <QtCore/QSettings>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFormLayout>
#include <QtGui/QLineEdit>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>

SettingsDialog::SettingsDialog(QWidget* parent)
    : QDialog(parent)
    , m_dataDirectoryLineEdit(0)
{
    setupInterface();
    load();
}

SettingsDialog::~SettingsDialog()
{

}

void SettingsDialog::accept()
{
    save();
    QDialog::accept();
}

void SettingsDialog::setupInterface()
{
    QVBoxLayout* mainLayout = new QVBoxLayout();
    setLayout(mainLayout);

    QWidget* formWidget = new QWidget();
    QFormLayout* formLayout = new QFormLayout();
    formWidget->setLayout(formLayout);
    mainLayout->addWidget(formWidget);

    m_dataDirectoryLineEdit = new QLineEdit(this);
    formLayout->addRow(tr("Data directory"), m_dataDirectoryLineEdit);

    m_photoBatchIntervalSpinBox = new QSpinBox(this);
    m_photoBatchIntervalSpinBox->setRange(1, 10);
    formLayout->addRow(tr("Photo batch interval"), m_photoBatchIntervalSpinBox);

    QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    mainLayout->addWidget(buttonBox);
}

void SettingsDialog::load()
{
    QSettings settings;
    m_dataDirectoryLineEdit->setText(settings.value("dataDirectory").toString());
    m_photoBatchIntervalSpinBox->setValue(settings.value("photoBatchInterval").toInt() / 1000);
}

void SettingsDialog::save()
{
    QSettings settings;
    settings.setValue("dataDirectory", m_dataDirectoryLineEdit->text());
    settings.setValue("photoBatchInterval", m_photoBatchIntervalSpinBox->value() * 1000);
}
