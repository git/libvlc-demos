#ifndef CLI_H
#define CLI_H

#include "Media.h"
#include <QSettings>
#include <QList>
#include <QFileInfo>
#include <QFile>
#include <QTemporaryFile>
#include <QDir>
#include <QStringList>
#include <QTextStream>
#include <QDateTime>
#include <iostream>
#include <cstring>
#include <cstdlib>

// Possible output formats.
class OutputFormat
{
public:
    static const int Default = 0x0,                         // Default format, equal to "Raw"
                     Raw = 0x1,                             // Output in plain text format.
                     Ini = 0x2;                             // Output in INI format.
};

// Class controlling the program in CLI-mode.
class Cli
{
public:
    Cli(int argc, char *argv[]);                            // Constructor evaluating command line parameters
    int exec();                                             // Execute application.
private:
    QList<Media *> m_media;                                 // List with information to media-files.
    bool m_usage;                                           // If true print only usage and quit program.
    QString m_filename;                                     // Name of the output file.
    int m_format;                                           // Output format.
    void printUsage() const;                                // Print command line options.
    int writeRaw();                                         // Write raw information of all media files to output file or stdout.
    void writeRaw(const Media& media, QFile& file);         // Write raw information of a single media file to output file or stdout.
    int writeIni();                                         // Write information in INI format of all media files to output file or stdout.
    void writeIni(const Media& media, QSettings& settings); // Write information in INI format of a single media file to output file or stdout.
};

#endif // CLI_H
